from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *
from .settings import apps_dir, style
from pathlib import Path
import re, os, shlex, traceback
from contextlib import redirect_stdout
import logging
from logging import handlers

logger = logging.getLogger('app_menu')
logger.setLevel(logging.INFO)
logger.addHandler(handlers.RotatingFileHandler(filename=(Path(apps_dir)/'main.log').as_posix(),
                                               maxBytes=1024*1024, backupCount=3))

logfile = apps_dir+'/log.txt'


class AppMenu(QObject):
    def __init__(self, *args):
        super(AppMenu, self).__init__(*args)

    def open_menu(self):
        files = list(Path(apps_dir).glob('*.desktop'))
        menu = AppMenuClass()
        for file in files:
            try:
                data = self.parse_file(file)
            except:
                logger.exception('File not opened')
                data = dict(
                    name=file.name,
                    icon=QIcon.fromTheme('dialog-error')
                )
            if data:
                act = QAction(data['name'], self)
                act.setData(data)
                if data.get('icon'):
                    act.setIcon(QIcon(data['icon']) if isinstance(data['icon'], str) else data['icon'])
                menu.addAction(act)
        menu.addSeparator()
        menu.addAction(QAction('Apps Dir', self, data='open_dir'))
        self._apply_style(menu)
        act = menu.exec_(QCursor.pos())
        if act:
            data = act.data()
            if not data:
                logger.warning('No data')
                return
            if data == 'open_dir':
                logger.info(f'Open {apps_dir}')
                os.popen('xdg-open '+apps_dir)
                return
            with open(logfile, 'a') as f:
                with redirect_stdout(f):
                    try:
                        self.start_app(data)
                    except Exception as e:
                        print(traceback.format_exc())
        QApplication.instance().quit()

    def _apply_style(self, menu):
        if os.path.exists(style):
            qss = open(style).read()
            menu.setStyleSheet(qss)

    def parse_file(self, file):
        text = file.open(encoding='utf-8').read()
        data = {}
        for line in text.split('\n'):
            m = re.search(r"^Name=(.*)", line.strip())
            if m:
                data['name'] = m.group(1)
            m = re.search(r"^Icon=(.*)", line.strip())
            if m:
                data['icon'] = m.group(1)
            m = re.search(r"^Exec=(.*)", line.strip())
            if m:
                data['exec'] = m.group(1)
            m = re.search(r"^Path=(.*)", line.strip())
            if m:
                data['cwd'] = m.group(1)
            m = re.search(r"^Terminal=(.*)", line.strip())
            if m:
                if m.group(1).lower() == 'true':
                    data['terminal'] = True
            data['path'] = str(file)

        if not data.get('name'):
            return
        return data

    def start_app(self, data):
        logger.info(f'Start app: {data}')
        from PySide2.QtCore import QProcess
        path = data['exec']
        cmd = shlex.split(path)
        cwd = data.get('cwd') or None
        if cwd:
            os.chdir(cwd)
        if data.get('terminal'):
            cmd = ['gnome-terminal', '--', 'bash',  '-c', "{}".format(' '.join(cmd))]
            logger.debug(' '.join(cmd))
            # cmd = ['gnome-terminal' ] + cmd
        QProcess.startDetached(' '.join(cmd))
        # o = subprocess.check_output(cmd, cwd=cwd)


class AppMenuClass(QMenu):
    def __init__(self, *args):
        super(AppMenuClass, self).__init__(*args)
        self.setStyle(MyProxyStyle())

    def focusOutEvent(self, event):
        QApplication.instance().quit()
        super(AppMenuClass, self).focusOutEvent(event)


class MyProxyStyle(QProxyStyle):
    pass
    def pixelMetric(self, QStyle_PixelMetric, option=None, widget=None):

        if QStyle_PixelMetric == QStyle.PM_SmallIconSize:
            return 24
        else:
            return QProxyStyle.pixelMetric(self, QStyle_PixelMetric, option, widget)
