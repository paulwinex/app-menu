import os


apps_dir = os.getenv('APP_MENU_DIR') or '/home/paul/.local/share/quick_apps'
style = os.path.join(os.path.dirname(__file__), 'menu_style.css')
