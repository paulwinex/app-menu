from PySide2.QtWidgets import *
from PySide2.QtGui import *
import sys
from .app import AppMenu


if __name__ == '__main__':
    try:
        app = QApplication([])
        m = AppMenu()
        m.open_menu()
        # app.exec_()
    except KeyboardInterrupt:
        sys.exit(0)
